package com.tlz.coolloadingview.example

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.btn_start
import kotlinx.android.synthetic.main.activity_main.btn_stop
import kotlinx.android.synthetic.main.activity_main.clv1
import kotlinx.android.synthetic.main.activity_main.clv2

class MainActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		btn_start.setOnClickListener {
			clv1.start()
			clv2.start()
		}
		btn_stop.setOnClickListener {
			clv1.stop()
			clv2.stop()
		}
	}
}
