package com.tlz.coolloadingview

/**
 * Created by Tomlezen.
 * Date: 2017/9/19.
 * Time: 下午9:54.
 */
data class Ball(var x: Float, var y: Float, var radius: Float, var initX: Float, var initY: Float)