package com.tlz.coolloadingview

import android.animation.Animator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator


/**
 * Created by Tomlezen.
 * Date: 2017/9/19.
 * Time: 下午9:35.
 */
class CoolLoadingView(context: Context, attrs: AttributeSet): View(context, attrs) {

	private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
	private val path = Path()
	private val centerPath = Path()

	private var sizeCenterBall: Float
	private var sizeBall: Float
	private var sizeBallDiff: Float
	private var sizeBallDist: Float

	private var colorBall: Int

	private var isInitFinish = false

  private var cycleTime: Long
	private var totalDist = 0f
	private var ballRadiusDist = 0f
  private var maxScrollDist = 0f
  private var centerBallStartH = 0f

	private val centerBall: Ball
	private val balls = mutableListOf<Ball>()

  private var drawnCenterX = 0f
  private var drawnCenterY = 0f

	private var leftAnimator: ValueAnimator? = null
	private var rightAnimator: ValueAnimator? = null
	private var animProgress = 0f
	private var isAnimRunning = false
  private var isStartLeft = false

	init {
		val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CoolLoadingView)
		colorBall = typedArray.getColor(R.styleable.CoolLoadingView_lvBallColor, Color.WHITE)
		sizeBall = typedArray.getDimensionPixelSize(R.styleable.CoolLoadingView_lvBallSize, resources.getDimensionPixelSize(R.dimen.def_ball_size)).toFloat()
    sizeCenterBall = typedArray.getDimensionPixelSize(R.styleable.CoolLoadingView_lvCenterBallSize, resources.getDimensionPixelSize(R.dimen.def_center_ball_size)).toFloat()
    sizeBallDiff = typedArray.getDimensionPixelSize(R.styleable.CoolLoadingView_lvBallSizeDiff, resources.getDimensionPixelSize(R.dimen.def_ball_size_diff)).toFloat()
    sizeBallDist = typedArray.getDimensionPixelSize(R.styleable.CoolLoadingView_lvBallDist, resources.getDimensionPixelSize(R.dimen.def_ball_dist)).toFloat()
    cycleTime = typedArray.getInteger(R.styleable.CoolLoadingView_lvCycleTime, 8000).toLong()
		typedArray.recycle()

		paint.color = colorBall

    maxScrollDist = sizeBallDist + sizeBall * 2
    centerBallStartH = sizeBall * 1.5f
		centerBall = Ball(0f, 0f, sizeCenterBall, 0f, 0f)
    balls.add(Ball(0f, 0f, sizeBall, 0f, 0f))
    balls.add(Ball(0f, 0f, sizeBall, 0f, 0f))

		isInitFinish = true
	}

  override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec))
  }

  private fun measureWidth(widthMeasureSpec: Int): Int{
    val specMode = View.MeasureSpec.getMode(widthMeasureSpec)
    val specSize = View.MeasureSpec.getSize(widthMeasureSpec)
    return  when(specMode){
      MeasureSpec.EXACTLY -> specSize
      MeasureSpec.UNSPECIFIED, MeasureSpec.AT_MOST -> {
        val size = ((sizeCenterBall + balls.size * (sizeBall + sizeBallDist)) * 2 + 20).toInt()
        if(specMode == MeasureSpec.UNSPECIFIED || size < specSize){
          size
        }else{
          specSize
        }
      }
      else -> specSize
    }
  }

  private fun measureHeight(heightMeasureSpec: Int): Int{
    val specMode = View.MeasureSpec.getMode(heightMeasureSpec)
    val specSize = View.MeasureSpec.getSize(heightMeasureSpec)
    return  when(specMode){
      MeasureSpec.EXACTLY -> specSize
      MeasureSpec.UNSPECIFIED, MeasureSpec.AT_MOST -> {
        val size = (sizeCenterBall * 2 + balls.size * sizeBallDiff + 20).toInt()
        if(specMode == MeasureSpec.UNSPECIFIED || size < specSize){
          size
        }else{
          specSize
        }
      }
      else -> specSize
    }
  }

  override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
    super.onLayout(changed, left, top, right, bottom)
		if(changed){
			drawnCenterX = width / 2f
			drawnCenterY = height / 2f

			centerBall.x = drawnCenterX
			centerBall.initX = drawnCenterX
			centerBall.y = drawnCenterY
			centerBall.initY = drawnCenterY

			balls.forEach { ball ->
        ball.x = drawnCenterX
        ball.initX = ball.x
        ball.y = drawnCenterY
        ball.initY = drawnCenterY
			}

      totalDist = (sizeCenterBall + sizeBall * 2 - sizeBallDiff) * balls.size
		}
  }

	override fun onDraw(canvas: Canvas?) {
		super.onDraw(canvas)
		canvas?.let {
			paint.color = colorBall
			it.drawCircle(centerBall.x, centerBall.y, centerBall.radius, paint)
			balls.forEach { (x, y, radius) ->
				it.drawCircle(x, y, radius, paint)
			}
			if(!path.isEmpty){
				it.drawPath(path, paint)
			}
      if(!centerPath.isEmpty){
        it.drawPath(centerPath, paint)
      }
		}
	}

	private fun calculate(){
		path.reset()
    centerPath.reset()
		val ballSize = balls.size
    balls.forEachIndexed { index, ball ->
      val start = (ballSize - index - 1f) / balls.size
			if(Math.abs(animProgress) > start){
        val dist: Float
        if(animProgress >= 0f){
          if(ball.initX <= centerBall.x){
            ball.initX = centerBall.x + sizeCenterBall + sizeBallDiff * (index + 1) - sizeBall
          }
					dist = (animProgress - start) * totalDist
        }else{
          if(ball.initX >= centerBall.x){
            ball.initX= centerBall.x - sizeCenterBall - sizeBallDiff * (index + 1) + sizeBall
          }
          dist = (animProgress + start) * totalDist
        }
        ball.x = ball.initX + dist
				val progress = Math.abs(dist) / (sizeBall * 2 - sizeBallDiff)
				if(progress <= 1f){
          centerBall.radius = sizeCenterBall + (index + 1 - progress) * sizeBallDiff
				}
        makePath(ball)
//        Log.d("x", "progress=$progress, index = $index, x = $ball")
			}
    }
		invalidate()
	}

	private fun makePath(ball: Ball){
		val isLeft = animProgress < 0f
    val dist = if(isLeft) centerBall.x - centerBall.radius - ball.x + ball.radius else ball.x + ball.radius - centerBall.x - centerBall.radius
    val isEnter = (isStartLeft && animProgress <= 0f) || (!isStartLeft && animProgress >= 0f)
		if(dist <= maxScrollDist){
      val x1: Float
      val y1: Float

      val x2: Float
      val y2: Float

      val x3: Float
      val y3: Float

      val x4: Float
      val y4: Float

      val anchorX: Float
      val anchorY: Float

      val offsetX = Math.sqrt(centerBall.radius * centerBall.radius - centerBallStartH * centerBallStartH.toDouble()).toFloat()
      when {
        dist < ball.radius -> {
          if(isEnter){
            return
          }
					val l1: Float
					val l2: Float
					if(isLeft){
            l1 = ball.x - centerBall.x + centerBall.radius
            x1 = centerBall.x - offsetX
            x3 = centerBall.x - centerBall.radius
					} else {
            l1 = ball.x - centerBall.x - centerBall.radius
            x1 = centerBall.x + offsetX
            x3 = centerBall.x + centerBall.radius
					}
          l2 = Math.sqrt(ball.radius * ball.radius - l1 * l1.toDouble()).toFloat()
          y1= centerBall.y - centerBallStartH
          x2 = x1
          y2= centerBall.y + centerBallStartH
          y3 = centerBall.y - l2
          x4 = x3
          y4= centerBall.y + l2
          anchorX = (x1 + x3) / 2
          path.moveTo(x1, y1)
          path.quadTo(anchorX, y3, x3, y3)
          path.lineTo(x4, y4)
          path.quadTo(anchorX, y4, x2, y2)
          path.lineTo(x1, y1)
        }
        dist < ball.radius * 2 + sizeBallDist / 2 -> {
          if(isEnter && dist > ball.radius * 2){
            return
          }
          val scale = (dist - ball.radius) / (ball.radius + sizeBallDist / 2)
          val h = ball.radius * (1 - 0.3f * scale)
          val offsetBallX = Math.sqrt(ball.radius * ball.radius - h * h.toDouble()).toFloat()
          if(isLeft){
            x1 = centerBall.x - offsetX
            x3 = ball.x + offsetBallX
          } else {
            x1 = centerBall.x + offsetX
            x3 = ball.x - offsetBallX
          }
          y1 = ball.y - centerBallStartH
          x2 = x1
          y2= ball.y + centerBallStartH
          y3 = ball.y - h
          x4 = x3
          y4= ball.y + h
          anchorX = (x1 + x3) / 2
          anchorY = ball.radius * 1.25f - scale * ball.radius * 1.75f
          path.moveTo(x1, y1)
          path.quadTo(anchorX, centerBall.y - anchorY, x3, y3)
          path.lineTo(x4, y4)
          path.quadTo(anchorX, centerBall.y + anchorY, x2, y2)
          path.lineTo(x1, y1)
        }
        !isEnter -> {
          val d = sizeBallDist / 2 * ((maxScrollDist - dist) / maxScrollDist) * 4
          val h = ball.radius * 0.7f
          val offsetBallX = Math.sqrt(ball.radius * ball.radius - h * h.toDouble()).toFloat()
          val anchorX1: Float
          val anchorX2: Float
          if(isLeft){
            x1 = centerBall.x - offsetX
            x3 = ball.x + offsetBallX
            anchorX1 = centerBall.x - centerBall.radius - d
            anchorX2 = ball.x + ball.radius + d
          } else {
            x1 = centerBall.x + offsetX
            x3 = ball.x - offsetBallX
            anchorX1 = centerBall.x + centerBall.radius + d
            anchorX2 = ball.x - ball.radius - d
          }
          y1 = ball.y - centerBallStartH
          x2 = x1
          y2= ball.y + centerBallStartH
          y3 = ball.y - h
          x4 = x3
          y4= ball.y + h
          centerPath.moveTo(x1, y1)
          centerPath.quadTo(anchorX1, centerBall.y, x2, y2)
          centerPath.lineTo(x1, x1)
          path.moveTo(x3, y3)
          path.quadTo(anchorX2, ball.y, x4, y4)
          path.lineTo(x3, x3)
        }
      }
		}
	}

	fun start(){
		startRight()
	}

  private fun startRight(){
    if(!isAnimRunning){
      if(rightAnimator == null){
        rightAnimator = ValueAnimator.ofFloat(1f, -1f)
            .apply {
              duration = cycleTime / 2
              startDelay = 300L
              interpolator = AccelerateDecelerateInterpolator()
              addUpdateListener {
                animProgress = animatedValue as Float
                calculate()
              }
              endWithAction {
                isAnimRunning = false
                startLeft()
              }
            }
      }
      isStartLeft = false
      rightAnimator?.start()
    }
  }

  private fun startLeft(){
    if(!isAnimRunning){
      if(leftAnimator == null){
        leftAnimator = ValueAnimator.ofFloat(-1f, 1f)
            .apply {
              duration = cycleTime / 2
              startDelay = 200L
              interpolator = AccelerateDecelerateInterpolator()
              addUpdateListener {
                animProgress = animatedValue as Float
                calculate()
              }
              endWithAction {
                isAnimRunning = false
                startRight()
              }
            }
      }
      isStartLeft = true
      leftAnimator?.start()
    }
  }

	fun stop(){
		leftAnimator?.end()
		rightAnimator?.end()
		isAnimRunning = false
	}

	private fun Animator.endWithAction(action: ()-> Unit){
    addListener(object: Animator.AnimatorListener{
      override fun onAnimationRepeat(p0: Animator?) {

      }

      override fun onAnimationEnd(p0: Animator?) {
        action()
      }

      override fun onAnimationCancel(p0: Animator?) {

      }

      override fun onAnimationStart(p0: Animator?) {

      }

    })
	}

}